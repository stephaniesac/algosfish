
from gensim.models import Word2Vec
from gensim.models.keyedvectors import KeyedVectors

WIKI_MODEL = "en_1000_no_stem/en.model"
WIKI_VECTORS = "wiki_vec"

from collections import Counter
import re
import spacy

def cleanText(text):
    text = re.sub(r"[\n \t\s]", '', text)
    text = re.sub(r"[\~\`\@\#\$\%\^\&\*\(\)\_\+\=\-\.\,\<\>\?\/\|\[\]\{\}]", '', text)
    return text

def topicSimilarity(word, counters, headings, linked_words, word_vectors):
    similarity = 0
    title = cleanText(headings[0])
    try:
        similarity += word_vectors.similarity(cleanText(word), title) #TODO word.similarity(w)
    except: pass

    total_words = 1

    '''Uncomment below to inclue weights for similarity with common words in related texts. '''
    # num_keyword = 15
    # for c in counters:
    #     for w in c.most_common(num_keyword):
    #         try:
    #             similarity += word_vectors.similarity(cleanText(word), cleanText(w)) #word.similarity(w)
    #             total_words += 1
    #         except: pass

    # for w in linked_words:
    #     a = cleanText(word)
    #     b = cleanText(w)
    #     try:
    #         similarity += 0.5*word_vectors.similarity(a, b) #word.similarity(w)
    #         total_words += 1
    #     except:
    #         print ("  Couldn't find similarity between a:" + repr(a)+ " b: "+repr(b))
    return similarity / total_words

def rankTokens(nlp, doc, headings, linked_words, related_docs, random_docs):
    # Count word frequency
    counters = []
    words = []
    for doc in (related_docs + [doc]):
        # Break down into just actual words.
        for token in doc:
            if token.is_stop != True and token.is_punct != True and \
                token.is_digit != True and token.is_space != True and \
                token.like_num != True and token.is_stop != True and \
                token.text != '\n' and token.text != '\t':
                if token.lemma_:
                    words.append(token.lemma_)
                else:
                    words.append(token.text)
        counters.append(Counter(words))

    '''Token breakdown instead of words, if vanilla Spacy is used. '''
    # Break down into tokens.
    # doc_tokens = []
    # linked_tokens = set()
    # for token in doc:
    #     if token.is_stop != True and token.is_punct != True and \
    #         token.is_digit != True and token.is_space != True and \
    #         token.like_num != True and token.is_stop != True and \
    #         token.text != '\n' and token.text != '\t':
    #         doc_tokens.append(token)
    #     if token.text in linked_words:
    #         linked_tokens.add(token)

    print("    Finished detecting tokens. Loading word vectors...")
    word_vectors = KeyedVectors.load(WIKI_VECTORS)
    print("    Finished loading Wiki Vectors.\n")

    rankings = {}
    for w in words:
        rankings[w] = topicSimilarity(w, counters, headings, linked_words, word_vectors)
        # print (w + " : " + str(rankings[w]))
    return rankings
