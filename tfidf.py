'''
    tfidf.py
'''

from collections import Counter
import math

def tf_idf(w, word_freq, linked_words, related_counters, random_counters, ents):
    # Calcuate basic TF
    tf = math.log(1 + word_freq[w])

    '''Uncomment the line below to include some weight for entities. '''
    # Emphasis on entities - note that this seems to skew results.
    # if w in ents:
    #     tf *= 1.15

    # If the word is linked/in subheadings, generally more important.
    if w in linked_words:
        tf *= 1.25

    # Get the num_keywords most common words from related texts.
    num_keywords = 15
    doc_freq = 0
    for c in related_counters:
        if w in c.most_common(num_keywords):
            doc_freq += 1
    tf *= math.log(math.exp(1) + doc_freq)

    '''Uncomment the following for idf; needs to be adjusted.'''
    # Calculate IDF
    # doc_freq = 0
    # for c in random_counters:
    #     if w in c: doc_freq += 1
    # idf = math.log((1+len(random_counters))/(1 + doc_freq))   # TODO when no random?
    return tf #*idf

def rankTokens(doc, linked_words, related_docs, random_docs):
    related_counters = []
    random_counters = []
    ents = [e.text for e in doc.ents]

    # Count word frequency
    for i in range(len(related_docs + random_docs)+1):
        # Determine what kind of text currently analysing
        if (i < len(related_docs)):
            t = related_docs[i]
            counters = related_counters
        elif (i < len(related_docs + random_docs)):
            t = random_docs[i-len(related_docs)]
            counters = random_counters
        else:
            t = doc
            counters = random_counters
        if not t: continue

        # Break down into just actual words.
        words = []
        for token in t:
            if token.is_stop != True and token.is_punct != True and \
                token.is_digit != True and token.is_space != True and \
                token.like_num != True and token.is_stop != True and \
                token.text != '\n' and token.text != '\t':
                if token.lemma_:
                    words.append(token.lemma_)
                else:
                    words.append(token.text)
        counters.append(Counter(words))
    word_freq = random_counters.pop()               # for main_doc

    print ("    Calculated word frequency for texts.")
    rankings = {}
    for w in words:
        rankings[w] = tf_idf(w, word_freq, linked_words, \
                             related_counters, random_counters, ents)
    return rankings
