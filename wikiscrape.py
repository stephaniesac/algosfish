'''
    wikiscrape.py

'''

from collections import Counter
from bs4 import BeautifulSoup, Tag
from spacy.symbols import *

import math
import matplotlib.pyplot as plt
import networkx as nx
import re
import spacy
import urllib.request

import tfidf
import word2vec
import sentence_analysis

TFIDF = 0
WIKI2VEC = 1
algo = TFIDF

# LOAD = False             # Flag to determine if should load from past data
PAST_DATA = "rank_data.txt"
WIKI_URL = "http://en.wikipedia.org"
RANDOM_URL = "https://en.wikipedia.org/wiki/Special:Random"

PREMADE_URLS = [
    "https://en.wikipedia.org/wiki/Thor:_Ragnarok",
    "https://en.wikipedia.org/wiki/Halloween",
    "https://en.wikipedia.org/wiki/Crime_in_Japan",
    "https://en.wikipedia.org/wiki/Paradise_Papers",
    "https://en.wikipedia.org/wiki/Landform",
    "https://en.wikipedia.org/wiki/Speech-language_pathology",
    "https://en.wikipedia.org/wiki/History_of_money",
    "https://en.wikipedia.org/wiki/Cooking",
    "https://en.wikipedia.org/wiki/Environmental_studies",
    "https://en.wikipedia.org/wiki/History_of_science_and_technology",
    "https://en.wikipedia.org/wiki/Firefighting",
    "https://en.wikipedia.org/wiki/Communication",
    "https://en.wikipedia.org/wiki/Management",
    "https://en.wikipedia.org/wiki/Sense_of_community",
    "https://en.wikipedia.org/wiki/Animism"
]

# Parses subheadings from the Wikipedia page and returns links.
def parseHeadingsWiki(soup, mindmap, headings, links, linked_words):
    # Parse the HTML.
    main_title = soup.find("h1").string
    mindmap.add_node(main_title)
    headings.append(main_title)
    soup.find("h1").decompose()         # TODO

    # Links from Categories
    for a in soup.find_all('a', href=True):
        l = a.get("href")
        if re.match("/wiki/Category:.*", l):
            l = WIKI_URL + '/wiki/' + re.sub("/wiki/Category:", '', l)
            links.add(l)
        # Also find words with links:
        elif re.match("/wiki/Special:.*", l):
            continue
        elif re.match("/wiki/Wikipedia.*", l):
            continue
        elif re.match("/wiki/Portal:.*", l):
            continue
        elif re.match("/wiki/Help.*", l):
            continue
        elif re.match("/wiki/File:.*", l):
            continue
        elif re.match("/wiki/.*", l):
            if (a.text == "ISBN"): continue
            linked_words.add(a.text)

    # Parse for h2-h3 subheadings.
    for h2 in soup.find_all("h2"):
        # Break if not found.
        if not h2: break

        # Find mw-headline which are article headings in Wikipedia.
        sp2 = h2.find_next("span", {"class":"mw-headline"})
        if not sp2: continue

        # Extract h2 title and add to edge.
        h2_title = sp2.string
        if (h2_title == "See also"):    # Obtain similar links
            ul = sp2.find_next("ul")
            for a in ul.find_all('a'):
                links.add(WIKI_URL + a.get('href'))
            break
        if (h2_title == "External links" or h2_title == "References"):
            break
        else:
            mindmap.add_edge(main_title, h2_title)
            headings.append(h2_title)
            # Parse for h3 subheadings.
            for h3 in h2.find_next_siblings():
                if not h3:
                    break
                # Break if the next h2 subheading is found.
                if isinstance(h3, Tag) and h3.name == "h2":
                    break
                if isinstance(h3, Tag) and h3.name == "h3":
                    # Extract h3 titles.
                    sp3 = h3.find_next("span", {"class":"mw-headline"})
                    if not sp3: continue
                    h3_title = sp3.string
                    mindmap.add_edge(h2_title, h3_title)
                    headings.append(h3_title)
                    sp3.decompose()     # TODO
            sp2.decompose()

# Clear out unnecessary Wikipedia clutter, turns into just sentences.
def wikiCleanText(soup):
    title = soup.find("title")
    if title: title.decompose()
    nav = soup.find("div", {"id": "jump-to-nav"})
    if nav: nav.decompose()
    subnote = soup.find("div", {"id": "siteSub"})
    if subnote: subnote.decompose()
    first_table = soup.find("tbody")
    if first_table: first_table.decompose()
    toc = soup.find("div", {"id": "toc"})
    if toc: toc.decompose()
    reflist = soup.find("div", {"class" : "reflist"})
    if reflist: reflist.decompose()
    footer = soup.find("div", {"id": "footer"})
    if footer: footer.decompose()

    for media in soup.find_all("div", {"class":"PopUpMediaTransform"}):
        if media: media.decompose()
    for caption in soup.find_all("div", {"class":"thumbcaption"}):
        if caption: caption.decompose()
    for n in soup.find_all("div", {"role":"note"}):
        if n : n.decompose()
    for sup in soup.find_all("sup"):
        if sup: sup.decompose()

    text_only = soup.get_text()
    text_only = text_only[0:text_only.find("References[edit]")]
    text_only = re.sub(r'.*document.documentElement.className = doc.*', '', text_only)
    text_only = re.sub(r'.*\(window.RLQ=.*', '', text_only)
    text_only = re.sub(r'mw.user.tokens.set.*', '', text_only)
    text_only = re.sub(r'.*}\);mw.loader.*','', text_only)
    text_only = re.sub(r'\[edit\]', '', text_only)
    text_only = re.sub(r'\[[0-9]{1,3}\]', '', text_only)
    text_only = re.sub(r'[\xa0]{1,}', ' ', text_only)
    text_only = re.sub(r'[\']', "'", text_only)
    text_only = re.sub(r'[ ]{2,}', ' ', text_only)
    text_only = re.sub(r'[\t ]{2,}', '\t', text_only)
    text_only = re.sub(r'[\n \t]{2,}', '\n', text_only)
    return text_only

# Treats chunks of related words as chunks of text.
def collapseData(doc):
    # Collapse hyphen punctuation.
    spans = []
    for word in doc[:-1]:
        if word.nbor(1).tag_ == "HYPH":
            span = doc[word.i : word.i+3]
            spans.append(
                (span.start_char, span.end_char, word.tag_, word.lemma_, word.ent_type_)
            )
    for span_props in spans:
        doc.merge(*span_props)

    # Merge adjectival modifiers with nouns
    spans = []
    for token in doc:
        if token.dep == amod or token.dep == prep:
            if token.nbor(1).pos_ == NOUN or token.nbor(1).pos_ == ADJ:
                span = doc[token.i : token.i+2]
                spans.append(
                    (span.start_char, span.end_char, token.tag_, token.lemma_, token.ent_type_)
                )
    for span_props in spans:
        doc.merge(*span_props)

    # Collapse noun phrases.
    for np in list(doc.noun_chunks):
        np.merge(np.root.tag_, np.text, np.root.ent_type_)

    # Collapse entity chunks.
    for ent in list(doc.ents):
        ent.merge(ent.root.tag_, ent.text, ent.root.ent_type_)

# Opens link and creates soup object.
def makeSoup(url):
    print("  Analysing data from " + url)
    try:
        page = urllib.request.urlopen(url).read()
    except:
        print("!!! Could not open url. ")
        return
    return BeautifulSoup(page, "html5lib")

# Return nlp-analysed doc from soup.
def analyseSoup(soup, nlp):
    if not soup: return
    text_only = wikiCleanText(soup)
    doc = nlp(text_only)
    if algo is not WIKI2VEC: collapseData(doc)
    return doc

def rankSentences(doc, token_ranks):
    sent_ranks = {}
    j = 0                   # Give emphasis to first 10 sentences
    for s in list(doc.sents):
        if not s: continue
        # Average token ranks within a sentence.
        sent_ranks[s] = 0.0
        for num_words, word in enumerate(s.text):
            if not word or word not in token_ranks: continue
            sent_ranks[s] += float(token_ranks[word])
        sent_ranks[s] /= float(num_words)

        # Gives emphasis to first 10 sentences
        if j < 5:
            sent_ranks[s] *= 1.5
            j += 1
    return sent_ranks

def main():
    url = "https://en.wikipedia.org/wiki/Bouldering"
    # url = "https://en.wikipedia.org/wiki/Rose-ringed_parakeet"
    print("WikiMaps initialising for " + url)

    mindmap = nx.DiGraph()
    headings = []
    links = set()
    linked_words = set()
    related_docs = []
    random_docs = []

    # Parse subheadings, related words and links.
    main_soup = makeSoup(url)
    parseHeadingsWiki(main_soup, mindmap, headings, links, linked_words)
    print("  Parsed subheadings.")

    # Start NLP analysis.
    nlp = spacy.load('en')
    main_doc = analyseSoup(main_soup, nlp)

    # Analyse related texts.
    for l in links:
        if l != url: related_docs.append(analyseSoup(makeSoup(l), nlp))

    # The following analyses random/premade urls for IDF, but removed as per report.
    # num_randoms = 5
    # for i in range(num_randoms):
    #     random_docs.append(analyseSoup(makeSoup(RANDOM_URL), nlp))
    # # Add from a set link of random topics
    # for l in PREMADE_URLS:
    #     if (l != url): random_docs.append(analyseSoup(makeSoup(l), nlp))

    # Start ranking tokens and sentences.
    if algo is WIKI2VEC:
        token_ranks = word2vec.rankTokens(nlp, main_doc, headings, linked_words, \
                                        related_docs, random_docs)
    else:
        token_ranks = tfidf.rankTokens(main_doc, linked_words, \
                                        related_docs, random_docs)
    sent_ranks = rankSentences(main_doc, token_ranks);

    # Print sentences and their rankings.
    print("======== PRINTING TOP 20 SENTENCES: ========")
    sents = []
    i = 0
    for key, value in sorted(token_ranks.items(), reverse=True, key=lambda x: x[1]):
        print(repr(key) +" : " + str(value))
        # i += 1
        sents.append(key)
        if (i > 20): break

    # Sentence analysis.
    for sent in sents:
        sentence_analysis.infoExtract(nlp, sent, mindmap, token_ranks)

    # Draw the mind map.
    nx.draw(mindmap, node_color="#ADD8E6", node_size=600, with_labels=True)
    plt.savefig("mindmap.png")
    plt.show()

main()
